# ns-3 tests for tsvwg scenarios

This repository contains scripts and results using
[ns-3](https://www.nsnam.org) to investigate potential
[issues](https://trac.ietf.org/trac/tsvwg/report/1?sort=ticket&asc=1&page=1)
raised by [IETF Transport Area Working Group](https://datatracker.ietf.org/group/tsvwg/about/)
participants about the L4S architecture.  

This repository currently focuses on [Issue #16 - Interaction w/ 3168-only ECN AQMs.](https://trac.ietf.org/trac/tsvwg/ticket/16) and [Issue #17 - Interaction w/ FQ AQMs.](https://trac.ietf.org/trac/tsvwg/ticket/17)   

Current simulation results for [issue 17, one flow](https://l4s.cablelabs.com/results.html) are posted on a CableLabs server.

The most recent changes to the code can be viewed from the 
[commit log](https://gitlab.com/tomhend/modules/l4s-evaluation/commits/master).

This simulation code can be parameterized to simulate and compare with selected results of 

* [Scenario 2 (single fq-codel)](https://github.com/heistp/sce-l4s-bakeoff#scenario-2),
* [Scenario 3 (single codel)](https://github.com/heistp/sce-l4s-bakeoff#scenario-3),
* [Scenario 5 (consecutive bottleneck](https://github.com/heistp/sce-l4s-bakeoff#scenario-5), and
* [Scenario 6 (consecutive bottleneck)](https://github.com/heistp/sce-l4s-bakeoff#scenario-6)

in the [testbed results](https://github.com/heistp/sce-l4s-bakeoff) posted by 
Pete Heist, which investigates a similar configuration, as well as related
[testbed results](https://l4s.cablelabs.com/l4s-testing/README.html) from Olivier Tilmans.

## Table of Contents

1. [Introduction](#toc_2)
2. [Simulation Setup](#toc_3)
3. [Simulation Outputs](#toc_4)
4. [Scenarios and Results](#toc_5)
5. [Installation](#toc_6)
6. [Usage](#toc_7)
7. [Future Work](#toc_8)

## Introduction

ns-3 provides an accessible framework for simulation-based experiments and allows for completely reproducible results, to complement testbed results.  ns-3 also provides a limited capability for inserting Linux kernel TCP code, which we have used for validation of results using native ns-3 models.

ns-3 models exist for some of the configurations under study in testbeds.
This repository extends the ns-3.30 release with additional models:

- TCP Cubic model derived from [RFC 8312](https://tools.ietf.org/html/rfc8312)
- DCTCP model derived from [RFC 8257](https://tools.ietf.org/html/rfc8257)
- Extensions to ns-3 CoDel and FQ-CoDel models for ECN marking support
- scripts to run scenarios of interest to tsvwg

This repository also contains scripts to automate the running and processing
of simulation scenarios proposed in the IETF transport area working group.

This repository will be extended in the near future.  The following were
worked on at IETF 106 hackathon and should be integrated soon:

- DualQ Coupled PI2 model based on the version -10 internet draft pseudocode
(current draft at:  https://gitlab.com/tomhenderson/ns-3-dev/tree/dual-queue)
- TCP Prague model derived from ns-3 DCTCP model with additional support for
  paced chirping.
- Additional experiment scripts

## Simulation Setup

The general simulation setup for issues 16 and 17 can be implemented in
ns-3 with the following generic topology:

```
// ---> downstream (primary data transfer from servers to clients)
// <--- upstream (return acks and ICMP echo response)
//
//  links:  (1)      (2)      (3)      (4)      (5)      (6)
//              ----     ----     ----     ----     ----
//  servers ---| WR |---| M1 |---| M2 |---| M3 |---| LR |--- clients
//              ----     ----     ----     ----     ----
//  ns-3 node IDs:
//  nodes 0-2    3        4        5        6         7       8-10
//
// The use of 'server' and 'client' terminology is consistent with RFC 1983
// terminology in that home clients are requesting data from Internet servers
//
// - The box WR is a WAN router, aggregating all server links
// - The box M1 is notionally an access network headend such as a CMTS or BRAS
// - The box M2 is notionally an access network CPE device such as a cable or DSL modem
// - The box M3 is notionally a home router (HR) running cake or FQ-CoDel
// - The box LR is another LAN router, aggregating all client links (to home devices)
// - Three servers are connected to WR, three clients are connected to LR
//
// clients and servers are configured for ICMP measurements and TCP throughput
// and latency measurements in the downstream direction
//
// Depending on the 'scenario', the middleboxes and endpoints will be
// configured differently.
```

The file `l4s-evaluation/examples/tsvwg-scenarios.cc` implements the
above topology.  The node depicted as M3 implements the rate shaping
on the notional home gateway, and the node depicted as M1 implements
rate shaping at a slightly higher rate.

In addition, the simulation configures one client-server pair for a
ping process (default 100ms interval) and provides two client-server
pairs for long running TCP-based file transfers.

A number of command line options are available for key configuration details,
including the ability to disable the M1 bottleneck (this is called a 
`control scenario`), to change the bottleneck link rates, to change 
the queueing discipline on M3, and to configure an IAQM response and
its delay threshold on CoDel.

## Simulation Outputs

The simulation is instrumented to output several traces into raw data files
(suffixed with `.dat`):

* ping RTT latency trace
* TCP cwnd and estimated RTT traces
* Time series of the file transfer throughput, measured over 200ms sampling intervals
* Length of the M1 and M3 queues, expressed in terms of milliseconds of queueing delay at the bottleneck link rate
* All drops and marks from queues M1 and M3
* Time series of the frequency of drops and marks over 100ms intervals

### Simulation Plots

Using Python matplotlib and pdfjam utilities, several Python programs and
a shell script (`plot.sh`) convert the raw data into a single-page PDF
with a number of subplots, an example of which is shown 
[here](https://l4s.cablelabs.com/results/6-dctcp-20191018-214743/6-dctcp_29_50Mbps_0.95_80ms.pdf).  This example shows single flow DCTCP performance across
the Reading across first by row and then by column, the
plots show (for single flow TCP experiments) plots of the ICMP RTT latency,
the M1 queue occupancy (in ms), a zoom of the M1 queue occupancy over a
shorter timescale (simulation time 5-10 seconds), the TCP RTT estimate,
the M3 queue occupancy (in ms), the number of CE marks per 100 ms, the
TCP throughput (measured at the application layer on the receiver), and
the TCP congestion window (in units of segments).

These plotting scripts are available in an `experiments` directory in 
the module, making it easier to configure, run, and post-process an 
experiment compared with running each simulation by hand and manually 
gathering and processing output data.

### Larger scale experiments

Additional Bash scripts are available to control simulation experiments
that sweep through multiple parameter value combinations.  The Bash
script `run-single.sh` will allow the user to set some initial variables
and automate the generation of the plot of a single run of the simulator.
The Bash scripts `sweep.sh` and `run-all.sh` build on this capability
to control the parametric sweep through multiple parameter values.

In the experiment directory
`l4s-evaluation/experiments/tsvwg-issue-17-one-flow`, the existing script
`run-all.sh` is configured to run 180 simulations and generate the HTML
page to render the results shown [here](https://l4s.cablelabs.com/results.html).
A `run-single.sh` is available to run one parameter combination.

The simulations are run with all combinations of the following parameter values:

* congestion controller: {reno, cubic, dctcp}
* FIFO rate (link3rate): {10Mbps, 20Mbps, 50Mbps, 100Mbps, 200Mbps}
* fq_codel shaper rate ratio (link5rateRatio):  {0.95, 0.9}
* base RTT: {0ms, 10ms, 20ms, 50ms, 80ms, 100ms}

In the experiment directory
`l4s-evaluation/experiments/tsvwg-issue-17-two-flows`, the existing script
`run-all.sh` is configured to run multiple simulations corresponding to the
two flow cases.  Results are not yet posted.
A `run-single.sh` is available to run one parameter combination.

The simulations are run with all combinations of the following parameter values:

* congestion controller: {'cubic-cubic', 'dctcp-dctcp', 'cubic-dctcp', 'dctcp-cubic'}
* FIFO rate (link3rate): {10Mbps, 20Mbps, 50Mbps, 100Mbps}
* fq_codel shaper rate ratio (link5rateRatio):  {0.95}
* base RTT: {0ms, 10ms, 20ms, 50ms, 80ms}

## Scenarios and Results

### Observations on tsvwg issue 17

The resulting 180 scenarios are plotted [here](https://l4s.cablelabs.com/results.html).
In all tested conditions, we see that the impact on the sparse ICMP flow from the presence of the TCP flow is largely the same regardless of which congestion controller is used.  We do not see a 4 second peak in the ICMP latency caused by the presence of DCTCP.  

We do see, as expected, that in low RTT cases the slow linear ramp-up in time of the CoDel marking rate results in several seconds of DCTCP latency that is greater than the CoDel threshold of 5ms. 

Additionally we see that, even in a situation in which the AQM algorithm is not ideal for the DCTCP congestion controller, DCTCP generally provides better throughput at lower latency than either cubic or reno.

### Comparison to "Scenario 6" by Pete Heist
The description of Pete Heist's Scenario 6 aligns with the Issue 17 experiment described by Jonathan Morton.

> This is similar to Scenario 5, but narrowed down to just the FIFO and CoDel
> combination.  Correct behaviour would show a brief latency peak caused by the
> interaction of slow-start with the FIFO in the subject topology, or no peak at
> all for the control topology; you should see this for whichever
> [RFC 3168](https://tools.ietf.org/html/rfc3168) flow is chosen as the control.
> Expected results with L4S in the subject topology, however, are a peak
> extending about 4 seconds before returning to baseline.

Our simulation results are most closely related to the ["L4S one-flow"](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/) results from Pete Heist. The topology in that case was labelled 'L4S' but is really about Prague or Cubic operating over FIFO and FQ-CoDel middleboxes (i.e. there is no L4S dualq-coupled-aqm nor any immediate AQM).  The topology in that experiment was:

> L4S: Sender → Delay → FIFO middlebox (bottleneck #1, 52.5Mbit) → FQ-AQM middlebox (bottleneck #2, 50Mbit) → L4S receiver

Pete's experiment used the following RTTs: ~0ms, 10ms, 80ms (implemented in the "Delay" element).

So our 50Mbps, 95% cases with RTTs of (0ms, 10ms, 80ms) are the most comparable to Pete's cases (which were 52.5Mbps instead of 50Mbps).

| Congestion Control | Bottleneck Rate / fq-codel Rate  (Mbps) | RTT (ms) | PH Experiment | NS3 Experiment |
|:-------------:|:-------------:|:-----:|:-----:|:-----:|
| cubic | 50/47.5 | 0 |  [batch-l4s-s6-1-cubic-50Mbit-0ms_var.png](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/batch-l4s-s6-1-cubic-50Mbit-0ms_var.png)  |  [6-cubic_25_50Mbps_0.95_0ms.pdf](https://l4s.cablelabs.com/results/6-cubic-20191018-205613/6-cubic_25_50Mbps_0.95_0ms.pdf)  |
| cubic | 50/47.5  | 10 |  [batch-l4s-s6-1-cubic-50Mbit-10ms_var.png](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/batch-l4s-s6-1-cubic-50Mbit-10ms_var.png)  |   [6-cubic_26_50Mbps_0.95_10ms.pdf]( https://l4s.cablelabs.com/results/6-cubic-20191018-205613/6-cubic_26_50Mbps_0.95_10ms.pdf) |
| cubic | 50/47.5  | 80 |  [batch-l4s-s6-1-cubic-50Mbit-80ms_var.png](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/batch-l4s-s6-1-cubic-50Mbit-80ms_var.png)  |    [6-cubic_29_50Mbps_0.95_80ms.pdf](https://l4s.cablelabs.com/results/6-cubic-20191018-205613/6-cubic_29_50Mbps_0.95_80ms.pdf) |
| dctcp | 50/47.5  | 0 | [batch-l4s-s6-1-dctcp-50Mbit-0ms_var.png](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/batch-l4s-s6-1-dctcp-50Mbit-0ms_var.png) |   [6-dctcp_25_50Mbps_0.95_0ms.pdf](https://l4s.cablelabs.com/results/6-dctcp-20191018-214743/6-dctcp_25_50Mbps_0.95_0ms.pdf) |
| dctcp | 50/47.5  | 10 |  [batch-l4s-s6-1-dctcp-50Mbit-10ms_var.png](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/batch-l4s-s6-1-dctcp-50Mbit-10ms_var.png)  | [6-dctcp_26_50Mbps_0.95_10ms.pdf](https://l4s.cablelabs.com/results/6-dctcp-20191018-214743/6-dctcp_26_50Mbps_0.95_10ms.pdf)   |
| dctcp | 50/47.5 | 80 |  [batch-l4s-s6-1-dctcp-50Mbit-80ms_var.png](https://www.heistp.net/downloads/sce-l4s-bakeoff/bakeoff-2019-09-13T045427-r1/l4s-s6-1/batch-l4s-s6-1-dctcp-50Mbit-80ms_var.png)  |    [6-dctcp_29_50Mbps_0.95_80ms.pdf](https://l4s.cablelabs.com/results/6-dctcp-20191018-214743/6-dctcp_29_50Mbps_0.95_80ms.pdf) |


## Installation

The installation relies on a slightly patched version of ns-3 until the
patch is integrated into the mainline of ns-3.

1) Install prerequisites for ns-3 on your workstation.  The minimal
   requirement for Linux is a C++ (g++ or clang++) compiler, Python 3, and git. 
   For macOS, install the Xcode command-line tools (or the full Xcode
   environment if you prefer).
   More details about various prerequisites for installation are provided
   on the [installation wiki](https://www.nsnam.org/wiki/Installation). 

2) To run the plotting scripts, Python Matplotlib and pdfjam are needed.
   Install with your applicable package manager.

3) Download the most recent release of ns-3: [ns-3.30.1](https://www.nsnam.org/releases/ns-allinone-3.30.1.tar.bz2).

4) Uncompress and cd into the top-level ns-3 directory at `ns-allinone-3.30.1/ns-3.30.1` 

5) We will install the `l4s-evaluation` module in the contributed code
   directory.  This module is on a separate GitLab.com repository.  Cd
into the `contrib` directory and clone from
   [GitLab.com](https://gitlab.com/tomhend/modules/l4s-evaluation).

```
$ cd contrib
$ git clone https://gitlab.com/tomhend/modules/l4s-evaluation
```

6) cd back one level to the top-level ns-3 directory, and patch the ns-3.30.1
   release with a patch provided in the `l4s-evaluation/patches/` directory.
   This patch adds support for ECE handling and DCTCP in the base TCP code,
   and adds ECN handling to CoDel and FqCoDel models.  

```
$ cd ..
$ patch -p1 -i contrib/l4s-evaluation/patches/patch-to-ns-3.30.1.patch 
```

You should see:

```
patching file src/internet/doc/tcp.rst
patching file src/internet/model/tcp-congestion-ops.cc
patching file src/internet/model/tcp-congestion-ops.h
patching file src/internet/model/tcp-socket-base.cc
patching file src/internet/model/tcp-socket-base.h
patching file src/internet/model/tcp-socket-state.h
patching file src/internet/test/tcp-advertised-window-test.cc
patching file src/internet/test/tcp-dctcp-test.cc
patching file src/internet/test/tcp-ecn-test.cc
patching file src/internet/test/tcp-general-test.cc
patching file src/traffic-control/model/codel-queue-disc.cc
patching file src/traffic-control/model/codel-queue-disc.h
patching file src/traffic-control/model/fq-codel-queue-disc.cc
patching file src/traffic-control/model/fq-codel-queue-disc.h
patching file src/traffic-control/model/queue-disc.cc
patching file src/traffic-control/model/queue-disc.h
```

7) Next, configure and build the program, and confirm that unit tests pass:

```
$ ./waf configure -d optimized --enable-examples --enable-tests
$ ./waf build
$ ./test.py 
```

At this point, you are ready to start using the l4s-evaluation program and
scripts.

## Usage

Users unfamiliar with ns-3 are encouraged to read the 
[ns-3 tutorial](https://www.nsnam.org/docs/release/3.30/tutorial/html/index.html) (also available in [PDF format](https://www.nsnam.org/docs/release/3.30/tutorial/ns-3-tutorial.pdf)) to get started.

Try running the main `tsvwg-scenarios.cc` program from the
command line, to output its help statement:

```
$ ./waf --run 'tsvwg-scenarios --PrintHelp'
```

You should see a listing of program options:

```
ns3.30.1-tsvwg-scenarios-optimized [Program Options] [General Arguments]

Program Options:
    --firstTcpType:                 First TCP type (cubic, dctcp, or reno) [cubic]
    --secondTcpType:                Second TCP type (cubic, dctcp, or reno) [cubic]
    --m3QueueType:                  M3 queue type (fq or codel) [fq]
    --baseRtt:                      base RTT [+80000000.0ns]
    --controlScenario:              control scenario (disable M1 bottleneck) [false]
    --useIaqm:                      useIaqm [false]
    --iaqmThreshold:                CoDel IAQM threshold [+1000000.0ns]
    --link3rate:                    data rate of link 3 for FIFO scenarios [50000000bps]
    --link5rateRatio:               ratio of data rate of link 5 to link 3 [0.95]
    --stopTime:                     simulation stop time [+70000000000.0ns]
    --enablePcap:                   enable Pcap [false]
    --pingTraceFile:                filename for ping tracing [tsvwg-scenarios-ping.dat]
    --firstTcpRttTraceFile:         filename for rtt tracing [tsvwg-scenarios-first-tcp-rtt.dat]
    --firstTcpCwndTraceFile:        filename for cwnd tracing [tsvwg-scenarios-first-tcp-cwnd.dat]
    --firstTcpThroughputTraceFile:  filename for throughput tracing [tsvwg-scenarios-first-tcp-throughput.dat]
    --m1DropTraceFile:              filename for m1 drops tracing [tsvwg-scenarios-m1-drops.dat]
    --m1DropsFrequencyTraceFile:    filename for m1 drop frequency tracing [tsvwg-scenarios-m1-drops-frequency.dat]
    --m1LengthTraceFile:            filename for m1 queue length tracing [tsvwg-scenarios-m1-length.dat]
    --m3MarkTraceFile:              filename for m3 mark tracing [tsvwg-scenarios-m3-marks.dat]
    --m3MarksFrequencyTraceFile:    filename for m3 mark frequency tracing [tsvwg-scenarios-m3-marks-frequency.dat]
    --m3DropTraceFile:              filename for m3 drop tracing [tsvwg-scenarios-m3-drops.dat]
    --m3LengthTraceFile:            filename for m3 queue length tracing [tsvwg-scenarios-m3-length.dat]

General Arguments:
    --PrintGlobals:              Print the list of globals.
    --PrintGroups:               Print the list of groups.
    --PrintGroup=[group]:        Print all TypeIds of group.
    --PrintTypeIds:              Print all TypeIds.
    --PrintAttributes=[typeid]:  Print all attributes of typeid.
    --PrintHelp:                 Print this help message.
```

The program can be run from the top-level directory, but some shell scripts
are available in the module experiments directory.  cd into the following
directory and run the `run-single.sh` script:

```
$ cd contrib/l4s-evaluation/experiments/tsvwg-issue-17-one-flow
$ ./run-single.sh
```

This will show that a single flow experiement using DCTCP, an 80 ms base RTT,
an M1 bottleneck link rate of 50 Mbps, and a link 5 to link 3 ratio of 0.95
(i.e. M3 bottleneck is 47.5 Mbps); this corresponds to Pete Heist's scenario
5 configuration.  Results will be stored in a timestamped `results` directory,
including data files and resulting PDF plots.

The script `run-all.sh` will execute the script `sweep.sh` and generate a
HTML page similar to the one posted on the 
[CableLabs L4S site](https://l4s.cablelabs.com/results.html).

## Future work

We plan to add example programs and experiments as needed.  We plan to add ns-3
models of dual queue coupled PI2 and TCP Prague, and to do further work
on CoDel's IAQM support. 

TCP Cubic and DCTCP models will be added to mainline ns-3 when ready.  We 
are performing further validation and writing unit tests before proposing
them to mainline.  The congestion window response to an ECE signal requires
further validation (although response to a loss will invoke PRR and
fast recovery, the current response to an ECE signal is to simply perform a
window halving (reno, dctcp) or beta reduction (cubic)).

For questions about the model, please use the
[ns-3-users Google Groups](https://groups.google.com/forum/?fromgroups#!forum/ns-3-users)
forum, and feel free to generate GitLab.com merge requests for proposed
patches.   All code contributions should follow 
[ns-3 guidelines](https://www.nsnam.org/develop/contributing-code/)
for contributed code.
