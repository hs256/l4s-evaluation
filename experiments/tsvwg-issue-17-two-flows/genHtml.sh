#!/bin/bash

# Copyright (c) 2019 Cable Television Laboratories, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions, and the following disclaimer,
#    without modification.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of the authors may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# Alternatively, provided that this notice is retained in full, this
# software may be distributed under the terms of the GNU General
# Public License ("GPL") version 2, in which case the provisions of the
# GPL apply INSTEAD OF those given above.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Output filename
file=results.html

#  Simulation Conditions

#bottleneck link rates
linkRates="10Mbps 20Mbps 50Mbps 80Mbps"

#fq_codel shaper rate ratio
ratios="0.95"

#Base RTTs
RTTs="0ms 10ms 20ms 50ms 80ms"

#find most recent results directories
scenario=('cubic-cubic' 'dctcp-dctcp' 'cubic-dctcp' 'dctcp-cubic')
dirs=""
for s in ${scenario[@]}
do
	 foo=`ls -tdx -w0 results/${s}* | cut -d" " -f1`
	 dirs="$dirs ${foo##*/}"
done

# Generate html front matter
cat <<EOF >$file
<!doctype html>
<html>
<head>
<title>Simulation Results</title>
<style type="text/css">
.myTable { background-color:#eee;border-collapse:collapse;width:100%; }
.myTable td, .myTable th { padding:5px;border:1px solid #000;text-align: center; }
</style>
</head>
<body>
EOF

for ratio in $ratios
do
echo "<h2>fq_codel rate shaper at $ratio of bottleneck</h2>"  >>$file
# Generate table
cat <<EOF >>$file
<table class="myTable"
<tr>
<th> </th>
EOF

## generate table header
for RTT in $RTTs
do
	echo "<th>$RTT</th>" >>$file
done
echo '</tr>' >>$file

## generate each row
for linkrate in $linkRates
do
echo '<tr>' >>$file
echo "<td>$linkrate</td>" >>$file
for RTT in $RTTs
do
echo '<td>'>>$file
count=0
for dir in $dirs
do
	fname=`ls results/$dir/*_${linkrate}_${ratio}_${RTT}.pdf` 
      
    echo "<a href=\"$fname\">${scenario[$count]}</a><br>" >>$file
    ((count++))

done
echo '</td>' >>$file
done  
echo '</tr>' >>$file
# done with row
done
echo '</table>' >>$file
# done with table
done
echo '</body></html>' >>$file
# done with file
