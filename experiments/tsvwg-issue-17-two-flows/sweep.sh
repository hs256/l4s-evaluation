#!/bin/bash

# Copyright (c) 2019 Cable Television Laboratories, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions, and the following disclaimer,
#    without modification.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of the authors may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# Alternatively, provided that this notice is retained in full, this
# software may be distributed under the terms of the GNU General
# Public License ("GPL") version 2, in which case the provisions of the
# GPL apply INSTEAD OF those given above.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Sweep through different values of base RTT, bottleneck link rate, and
# bottleneck rate ratio, for a given scenario configuration and provided
# TCP model
#
#  Usage:    ./sweep.sh firstTcpType secondTcpType
#
#  valid tcpType are:
#    reno
#    cubic
#    dctcp
#
#  other configuration choices (e.g. IAQM) can be set below
#

pathToTopLevelDir="../../../.."
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/${pathToTopLevelDir}/build/lib

export heading="Issue 17, two flow"
export m3QueueType="fq"
export useIaqm=0
export iaqmThreshold="1ms"
export controlScenario=0
export RngRun=1
export saveDatFiles=false
export firstTcpType=${1:-dctcp}
export secondTcpType=${2:-dctcp}
export summaryHeader="results directory: ${firstTcpType} ${secondTcpType}" 

export numSims=8 # number of simultaneous simulations to run. Set this equal to the number of cores on your machine

./waf build > /dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "Waf build faild"
	exit 1
fi
resultsDir=results/$firstTcpType-$secondTcpType-`date +%Y%m%d-%H%M%S`
mkdir -p ${resultsDir}
repositoryVersion=`git rev-parse --abbrev-ref HEAD`
repositoryVersion+=' commit '
repositoryVersion+=`git rev-parse --short HEAD`
repositoryVersion+=' '
repositoryVersion+=`git log -1 --format=%cd`
echo $repositoryVersion > ${resultsDir}/version.txt
gitDiff=`git diff`
if [[ $gitDiff ]]
then
	echo "$gitDiff" >> ${resultsDir}/version.txt
fi
PROFILE=$(./waf --check-profile | tail -1 | awk '{print $NF}')
VERSION=$(cat ../../../../VERSION | tr -d '\n')
EXECUTABLE_NAME=ns${VERSION}-tsvwg-scenarios-${PROFILE}
EXECUTABLE=${pathToTopLevelDir}/build/contrib/l4s-evaluation/examples/${EXECUTABLE_NAME}
if [ -f "$EXECUTABLE" ]; then
	cp ${EXECUTABLE} ${resultsDir}/tsvwg-scenarios
else
	echo "$EXECUTABLE not found, exiting"
	exit 1
fi
cp $0 ${resultsDir}/.
cp *.py ${resultsDir}/.
cp plot.sh ${resultsDir}/.
cd ${resultsDir}

function run-scenario () { 

	# process function arguments
	scenario_id=${1}
	link3rate=${2}
	link5rateRatio=${3}
	rtt=${4}

	echo starting scenario $scenario_id: ${firstTcpType} ${secondTcpType} ${link3rate} ${link5rateRatio} ${rtt}

	mkdir ${scenario_id}
	cp *.py ${scenario_id}/.
	cp plot.sh ${scenario_id}/.

	pingTraceFile=${scenario_id}/ping-rtt.dat
	firstTcpRttTraceFile=${scenario_id}/first-tcp-rtt.dat
	firstTcpCwndTraceFile=${scenario_id}/first-tcp-cwnd.dat
	firstTcpThroughputTraceFile=${scenario_id}/first-tcp-throughput.dat
	secondTcpRttTraceFile=${scenario_id}/second-tcp-rtt.dat
	secondTcpCwndTraceFile=${scenario_id}/second-tcp-cwnd.dat
	secondTcpThroughputTraceFile=${scenario_id}/second-tcp-throughput.dat
	m1DropTraceFile=${scenario_id}/m1-drops.dat
	m1DropsFrequencyTraceFile=${scenario_id}/m1-drops-frequency.dat
	m1LengthTraceFile=${scenario_id}/m1-length.dat
	m3MarkTraceFile=${scenario_id}/m3-marks.dat
	m3MarksFrequencyTraceFile=${scenario_id}/m3-marks-frequency.dat
	m3DropTraceFile=${scenario_id}/m3-drops.dat
	m3LengthTraceFile=${scenario_id}/m3-length.dat

	./tsvwg-scenarios \
	    --controlScenario=${controlScenario} \
	    --firstTcpType=${firstTcpType} \
	    --secondTcpType=${secondTcpType} \
	    --m3QueueType=$m3QueueType \
	    --useIaqm=$useIaqm \
	    --iaqmThreshold=$iaqmThreshold \
	    --link3rate=$link3rate \
	    --link5rateRatio=$link5rateRatio \
	    --baseRtt=$rtt \
	    --pingTraceFile=$pingTraceFile \
	    --firstTcpRttTraceFile=$firstTcpRttTraceFile \
	    --firstTcpCwndTraceFile=$firstTcpCwndTraceFile \
	    --firstTcpThroughputTraceFile=$firstTcpThroughputTraceFile \
	    --secondTcpRttTraceFile=$secondTcpRttTraceFile \
	    --secondTcpCwndTraceFile=$secondTcpCwndTraceFile \
	    --secondTcpThroughputTraceFile=$secondTcpThroughputTraceFile \
	    --m1DropTraceFile=$m1DropTraceFile \
	    --m1DropsFrequencyTraceFile=$m1DropsFrequencyTraceFile \
	    --m1LengthTraceFile=$m1LengthTraceFile \
	    --m3MarkTraceFile=$m3MarkTraceFile \
	    --m3MarksFrequencyTraceFile=$m3MarksFrequencyTraceFile \
	    --m3DropTraceFile=$m3DropTraceFile \
	    --m3LengthTraceFile=$m3LengthTraceFile \
	    --RngRun=${RngRun}

	cd ${scenario_id}
	fname=${firstTcpType}_${secondTcpType}_${scenario_id}_${link3rate}_${link5rateRatio}_${rtt}
	./plot.sh ../$fname >log.txt 2>&1
	rm *.py plot.sh
	cd ..

	if ! $saveDatFiles
	then
		rm -rf ${scenario_id}
	fi

	echo finished scenario $scenario_id
	
}
export -f run-scenario

#  Simulation Conditions

#bottleneck link rates
linkRates="10Mbps 20Mbps 50Mbps 80Mbps"

#fq_codel shaper rate ratio
ratios="0.95"

#Base RTTs
RTTs="0ms 10ms 20ms 50ms 80ms"

if [ -x "$(command -v parallel)" ]; then
	s=0
	(
		for link3Rate in $linkRates
		do
			for rateRatio in $ratios 
			do
				for RTT in $RTTs
				do
					let s=s+1
					printf '%s %s %s %s\n' "$s" "$link3Rate" "$rateRatio" "$RTT"
				done
			done
		done
	) | parallel --no-notice --colsep '\s+' -u run-scenario {}
else
	s=0
	for link3Rate in $linkRates
	do
		for rateRatio in $ratios 
		do
			for RTT in $RTTs
			do
				let s=s+1
		 		run-scenario "$s" "$link3Rate" "$rateRatio" "$RTT" &
		 		[[ $(( s % $numSims)) -eq 0 ]] && wait # pause every numSims until those jobs complete
			done
		done
	done
fi

wait

exit
