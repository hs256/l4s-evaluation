#!/usr/bin/env python2

# Copyright (c) 2019 Cable Television Laboratories, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions, and the following disclaimer,
#    without modification.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The names of the authors may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# Alternatively, provided that this notice is retained in full, this
# software may be distributed under the terms of the GNU General
# Public License ("GPL") version 2, in which case the provisions of the
# GPL apply INSTEAD OF those given above.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import sys

# Estimate delay is argv1

filename = 'm1-length.pdf'
filename2 = 'm1-length-zoom.pdf'

f = open(sys.argv[1])
t = []
t2 = []
latency = []
for line in f:
    columns = line.split()
    t.append(float(columns[0]))
    latency.append (float(columns[1]))
f.close()

#df= pd.DataFrame({'x1': enqueue, 'x2' : dequeue, 'y': cum_bytes})
title = "M1 queue occupancy (ms)"
plt.xlabel('Time (s)')
plt.ylabel('queue occupancy (ms)')
plt.ylim([0,250])
#plt.xlim([5,10])
plt.title(title, fontdict = {'fontsize' : 20})
plt.plot(t, latency, marker='', color='black')
plt.ticklabel_format(useOffset=False)
#plt.show()
plt.savefig(filename, format='pdf')
plt.close()


title = "M1 queue occupancy (ms) - zoom"
plt.xlabel('Time (s)')
plt.ylabel('queue occupancy (ms)')
plt.ylim([0,250])
plt.xlim([5,20])
plt.title(title, fontdict = {'fontsize' : 20})
plt.plot(t, latency, marker='', color='black')
plt.ticklabel_format(useOffset=False)
#plt.show()
plt.savefig(filename2, format='pdf')
plt.close()
